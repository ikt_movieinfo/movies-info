﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FilmInfo.Movie_Class
{
    public class Movie
    {
        public string rating { get; set; }
        public string genre { get; set; }
        public string title { get; set; }
        public int running_time { get; set; }
        public string directors { get; set; }
        public string actors { get; set; }
        public string year { get; set; }
        public string description { get; set; } //plot
        public int number_of_votes { get; set; }

    }
}