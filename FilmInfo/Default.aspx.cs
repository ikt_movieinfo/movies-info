﻿using FilmInfo.Movie_Class;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FilmInfo
{
    public partial class _Default : Page
    {
        string zanr_cel, zanr, naslov, sodrzina, godina, actori, direktori, rejting, dolzina_film, metascore, votes, urlIMDB, imgPoster, izglasenaOD;
        protected void Page_Load(object sender, EventArgs e)
        {
            #region Serialize Object


            var apiUrl = File.OpenText(@"D:\Krste\movies-info\FilmInfo\Top.JSON");
            string st = apiUrl.ReadToEnd();



            JArray jsonArray = JArray.Parse(st);
            foreach (JObject o in jsonArray.Children<JObject>())
            {

                OleDbConnection cn = new OleDbConnection();
                cn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
                string sqlString = "INSERT INTO TOP_M (Name,Rating,Metascore,Plot,Rated,ImbdUrl,UrlPoster,Votes,YearOf,Genre) VALUES  (@Name,@Rating,@Metascore,@Plot,@Rated,@ImbdUrl,@UrlPoster,@Votes,@YearOf,@Genre)";

                try
                {
                    cn.Open();

                    OleDbCommand komanda = new OleDbCommand(sqlString, cn);


                    foreach (JProperty p in o.Properties())
                    {




                        string name = p.Name;
                        string value = p.Value.ToString();
                        
                        if (name.Equals("title"))
                        {
                            naslov = value;
                            //Response.Write(naslov);
                            //komanda.Parameters.AddWithValue("@Title", naslov);
                        }
                        if (name.Equals("rating"))
                        {
                            rejting = value;
                            //Response.Write(naslov);
                            //komanda.Parameters.AddWithValue("@Title", naslov);
                        }
                        if (name.Equals("metascore"))
                        {
                            metascore = value;
                            //Response.Write(naslov);
                            //komanda.Parameters.AddWithValue("@Title", naslov);
                        }
                        if (name.Equals("simplePlot"))
                        {
                            sodrzina = value;
                            //Response.Write(naslov);
                            //komanda.Parameters.AddWithValue("@Title", naslov);
                        }
                        if (name.Equals("rated"))
                        {
                            izglasenaOD = value;
                            //Response.Write(naslov);
                            //komanda.Parameters.AddWithValue("@Title", naslov);
                        }
                        if (name.Equals("urlIMDB"))
                        {
                            urlIMDB = value;
                            //Response.Write(naslov);
                            //komanda.Parameters.AddWithValue("@Title", naslov);
                        }
                        if (name.Equals("urlPoster"))
                        {
                            imgPoster = value;
                            //Response.Write(naslov);
                            //komanda.Parameters.AddWithValue("@Title", naslov);
                        }
                        if (name.Equals("votes"))
                        {
                            votes = value;
                            //Response.Write(naslov);
                            //komanda.Parameters.AddWithValue("@Title", naslov);
                        }
                        if (name.Equals("year"))
                        {
                            godina = value;
                            //Response.Write(naslov);
                            //komanda.Parameters.AddWithValue("@Title", naslov);
                        }
                        if (name.Equals("genres"))
                        {

                            zanr_cel = value;
                                                JArray jsonArray1 = JArray.Parse(zanr_cel);
                                                for (int i = 0; i < jsonArray1.Count; i++)
                                                {

                                                    if (i == jsonArray1.Count - 1)
                                                        zanr += jsonArray1[i] + "";
                                                    //Response.Write(jsonArray1[i]+",");
                                                    else
                                                        zanr += jsonArray1[i] + ",";
                                                    //Response.Write(jsonArray1[i]);
                                                }
                        }
                    }


                    komanda.Parameters.AddWithValue("@Name", naslov);
                    komanda.Parameters.AddWithValue("@Rating", rejting);
                    komanda.Parameters.AddWithValue("@Metascore", metascore);
                    komanda.Parameters.AddWithValue("@Plot", sodrzina);
                    komanda.Parameters.AddWithValue("@Rated", izglasenaOD);
                    komanda.Parameters.AddWithValue("@ImbdUrl", urlIMDB);
                    komanda.Parameters.AddWithValue("@UrlPoster", imgPoster);
                    komanda.Parameters.AddWithValue("@Votes", votes);
                    komanda.Parameters.AddWithValue("@YearOf", godina);
                    komanda.Parameters.AddWithValue("@Genre", zanr);
                    komanda.ExecuteNonQuery();
                    komanda.Parameters.Clear();
                    komanda.Dispose();
                    komanda = null;
                    zanr = "";
                }
                catch (Exception err)
                {

                    lblPoraka.Text = err.Message;
                }
                finally
                {
                    cn.Close();
                }
            }
            ////foreach (var item in jsonArray)
            ////{

            ////    Response.Write(item.ToString());
            ////}



            //foreach (JObject o in jsonArray.Children<JObject>())
            //{


            //    foreach (JProperty p in o.Properties())
            //    {
            //        OleDbConnection cn = new OleDbConnection();
            //        cn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            //        string sqlString = "INSERT INTO MOVIE (Title,Genres,Rating,RunningTime,NumberVotes,Actors,YearOf,Plot) VALUES  (@Title,@Genres,@Rating,@RunningTime,@NumberVotes,@Actors,@YearOf,@Plot)";

            //        try
            //        {
            //            cn.Open();

            //            OleDbCommand komanda = new OleDbCommand(sqlString, cn);



            //            string name = p.Name;
            //            string value = p.Value.ToString();

            //            if (name.Equals("Title"))
            //            {
            //                naslov = value;
            //                //Response.Write(naslov);
            //                //komanda.Parameters.AddWithValue("@Title", naslov);
            //            }
            //            else


            //                if (name.Equals("Genres"))
            //                {
            //                    zanr_cel = value;
            //                    JArray jsonArray1 = JArray.Parse(zanr_cel);
            //                    for (int i = 0; i < jsonArray1.Count; i++)
            //                    {

            //                        if (i == jsonArray1.Count - 1)
            //                            zanr += jsonArray1[i] + "";
            //                        //Response.Write(jsonArray1[i]+",");
            //                        else
            //                            zanr += jsonArray1[i] + ",";
            //                        //Response.Write(jsonArray1[i]);


            //                    }
            //                    //komanda.Parameters.AddWithValue("@Genres", zanr);
            //                    //JObject k = JObject.Parse(value);
            //                    //for (int i = 0; i < value.Length; i++)
            //                    //{
            //                    //    Response.Write(string.Format("{0}", k["Genre"][i] + Environment.NewLine));
            //                    //}

            //                }
            //                else


            //                    if (name.Equals("Rating"))
            //                    {
            //                        rejting = value;
            //                        //komanda.Parameters.AddWithValue("@Rating", rejting);


            //                    }
            //                    else



            //                        if (name.Equals("Year"))
            //                        {
            //                            godina = value;
            //                            Response.Write(godina);
            //                        }
            //                        else


            //                            if (name.Equals("Plot"))
            //                            {
            //                                sodrzina = value;
            //                                //komanda.Parameters.AddWithValue("@Plot", sodrzina);
            //                            }
            //                            else
            //                            {
            //                                naslov = " "; zanr = ""; rejting = " "; godina = " "; sodrzina = " ";
            //                            }

            //            komanda.Parameters.AddWithValue("@Title", naslov);
            //            komanda.Parameters.AddWithValue("@Genres", zanr);
            //            komanda.Parameters.AddWithValue("@Rating", rejting);
            //            komanda.Parameters.AddWithValue("@RunningTime", "");
            //            komanda.Parameters.AddWithValue("@NumberVotes", "");
            //            komanda.Parameters.AddWithValue("@Actors", "");
            //            komanda.Parameters.AddWithValue("@YearOf", godina);
            //            komanda.Parameters.AddWithValue("@Plot", sodrzina);
            //            komanda.ExecuteNonQuery();
            //            komanda.Parameters.Clear();
            //            komanda.Dispose();
            //            komanda = null;
            //            zanr = "";
            //            //var movie = new Movie(){
            //            //     genre=zanr,
            //            //     title=naslov
            //            // };

            //            //var resultObject = Newtonsoft.Json.JsonConvert.SerializeObject(movie);
            //            //Response.Write(resultObject);
            //            //Response.Write(name + ": " + value);


            //        }
            //        catch (Exception err)
            //        {

            //            lblPoraka.Text = err.Message;
            //        }
            //        finally
            //        {
            //            cn.Close();
            //        }

            //    }

            //}

            ////var movie = new Movie()
            ////{
            ////    rating="asd",
            ////    genre="Comedy",
            ////    description="bla bla bla"
            ////};
            ////var resultObject = Newtonsoft.Json.JsonConvert.SerializeObject(movie);
            ////Response.Write("KCO json" + resultObject);


            #endregion


        }
    }
}