﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace FilmInfo
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogIn_Click(object sender, EventArgs e)
        {
            OleDbConnection cn = new OleDbConnection();
            cn.ConnectionString = ConfigurationManager.ConnectionStrings["ConnectionString"].ConnectionString;
            string sqlString = "SELECT Count(*) FROM LOGIN " +
                                   "WHERE username = @Username AND password = @Password";

            OleDbCommand cmd = new OleDbCommand(sqlString, cn);
            {
                cn.Open();
                cmd.Parameters.AddWithValue("@Username", txtUserName.Text);
                cmd.Parameters.AddWithValue("@Password", txtPassword.Text);
                int result = (int)cmd.ExecuteScalar();
                ElCommo pomosna = new ElCommo();
                if (result > 0)
                {
                   
                    pomosna.LogiranUser = 1;
                    User u = new User();
                    u.Role = "User";
                    u.Username = txtUserName.Text;
                    //string username = txtUserName.Text.Trim();
                    if (txtUserName.Text.Trim().Equals("admin@gmail.com"))
                    {
                        u.Role = "Admin";

                        Session["Username"] = u;


                        Response.Redirect("Default.aspx");

                    }
                    else
                    {
                        Session["Username"] = u;
                        Response.Redirect("Default.aspx");
                    }
                }
                else
                {
                    pomosna.LogiranUser = 0;
                    string script = "alert(\"Корисникот не е пронајден! Проверето го Корисничкото име и Лозинката!\");";
                    ScriptManager.RegisterStartupScript(this, GetType(),
                                          "ServerControlScript", script, true);
                }
                cn.Close();

            }
        }
    }
}