﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FilmInfo.Startup))]
namespace FilmInfo
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
